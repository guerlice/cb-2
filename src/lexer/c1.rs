use logos::Logos;

#[derive(Logos, Debug, PartialEq)]
pub enum C1Token {
    // Keywords
    #[token("bool")]
    KwBoolean,

    #[token("do")]
    KwDo,

    #[token("else")]
    KwElse,

    #[token("float")]
    KwFloat,

    #[token("for")]
    KwFor,

    #[token("if")]
    KwIf,

    #[token("int")]
    KwInt,

    #[token("printf")]
    KwPrintf,

    #[token("return")]
    KwReturn,

    #[token("void")]
    KwVoid,

    #[token("while")]
    KwWhile,

    // Operators
    #[token("+")]
    Plus,

    #[token("-")]
    Minus,

    #[token("*")]
    Asterisk,

    #[token("/")]
    Slash,

    #[token("=")]
    Assign,

    #[token("==")]
    Eq,

    #[token("!=")]
    Neq,

    #[token("<")]
    Lss,

    #[token(">")]
    Grt,

    #[token("<=")]
    Leq,

    #[token(">=")]
    Geq,

    #[token("&&")]
    And,

    #[token("||")]
    Or,

    // Other tokens
    #[token(",")]
    Comma,

    #[token(";")]
    Semicolon,

    #[token("(")]
    LParen,

    #[token(")")]
    RParen,

    #[token("{")]
    LBrace,

    #[token("}")]
    RBrace,

    // TermVariablen
    #[regex(r"[0-9]+", priority = 1)]
    ConstInt,

    #[regex(r"([0-9]+\.[0-9]+|\.[0-9]+)([eE]([-+])?[0-9]+)?|([0-9]+[eE]([-+])?[0-9]+)", priority = 2)]
    ConstFloat,

    #[regex(r"(true|false)")]
    ConstBoolean,

    #[regex(r#""[^"\n]*""#)]
    ConstString,

    #[regex(r"([a-zA-Z])+([0-9]|[a-zA-Z])*")]
    Id,

    #[regex(r"/\*[^*/]*\*/", logos::skip, priority = 5)] // C Comments
    #[regex(r"//[^\n]*", logos::skip, priority = 4)] // C++ Comments
    #[regex(r"[\\]", logos::skip)] // Backslash
    #[regex(r"[ \t\n\f\r:]+", logos::skip, priority = 3)] // Whitespaces, Newline...
    Ignored,

    // Logos requires one token variant to handle errors,
    // it can be named anything you wish.
    #[error]
    #[token("&")]
    #[token("|")]
    #[token("!")]
    Error,
}
