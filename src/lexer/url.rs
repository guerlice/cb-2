use logos::{Lexer, Logos, Source};
use std::fmt::{Display, Formatter};

/// Tuple struct for link URLs
#[derive(Debug, PartialEq)]
pub struct LinkUrl(String);

/// Implement Display for printing
impl Display for LinkUrl {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Tuple struct for link texts
#[derive(Debug, PartialEq)]
pub struct LinkText(String);

/// Implement Display for printing
impl Display for LinkText {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Token enum for capturing of link URLs and Texts
#[derive(Logos, Debug, PartialEq)]
pub enum URLToken {
    #[regex("<a [^>]*href=\"[^>]*>[^<]*</a\\s*>", extract_link_info)]
    Link((LinkUrl, LinkText)),
    
    #[regex("[^<]*", logos::skip)]
    #[regex("<[^>]*>", logos::skip)]
    Ignored,

    #[error]
    Error,
}

/// Extracts the URL and text from a string that matched a Link token
fn extract_link_info(lex: &mut Lexer<URLToken>) -> (LinkUrl, LinkText) {
    let text = lex.slice();
    let mut href_index = 0;
    let mut href_end_index = 0;
    let mut href_found = false;
    let mut href_end_found = false;

    let mut link_text_index = 0;
    let mut link_text_end_index = 0;
    let mut link_text_found = false;

    for (i, c) in text.chars().enumerate() {
        if !href_end_found {
            if c == 'h' && !href_found {
                let href = &text[i..i + 6];
                if href == "href=\"" {
                    href_index = i + 6;
                    href_found = true;
                }
            } else if href_found && i > href_index {
                if c == '\"' {
                    href_end_index = i;
                    href_end_found = true;
                }
            }
        } else {
            if c == '>' && !link_text_found {
                link_text_index = i + 1;
                link_text_found = true;
            } else if c == '<' && link_text_found {
                link_text_end_index = i;
                break;
            }
        }
    }

    (
        LinkUrl(String::from(&text[href_index..href_end_index])),
        LinkText(String::from(&text[link_text_index..link_text_end_index])),
    )
}
